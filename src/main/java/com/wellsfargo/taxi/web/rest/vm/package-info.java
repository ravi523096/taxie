/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wellsfargo.taxi.web.rest.vm;
