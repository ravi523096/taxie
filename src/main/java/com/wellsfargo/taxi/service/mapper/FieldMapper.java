package com.wellsfargo.taxi.service.mapper;

import com.wellsfargo.taxi.domain.*;
import com.wellsfargo.taxi.service.dto.FieldDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Field and its DTO FieldDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FieldMapper extends EntityMapper<FieldDTO, Field> {



    default Field fromId(Long id) {
        if (id == null) {
            return null;
        }
        Field field = new Field();
        field.setId(id);
        return field;
    }
}
