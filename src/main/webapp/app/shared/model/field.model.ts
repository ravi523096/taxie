import { Moment } from 'moment';

export interface IField {
    id?: number;
    type?: string;
    value?: string;
    updatedDate?: Moment;
    description?: string;
    updatedBy?: string;
}

export class Field implements IField {
    constructor(
        public id?: number,
        public type?: string,
        public value?: string,
        public updatedDate?: Moment,
        public description?: string,
        public updatedBy?: string
    ) {}
}
