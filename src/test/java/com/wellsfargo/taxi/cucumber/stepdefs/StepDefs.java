package com.wellsfargo.taxi.cucumber.stepdefs;

import com.wellsfargo.taxi.TaxieApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = TaxieApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
